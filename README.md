# README

[cmake]: https://cmake.org/
[cmocka]: https://cmocka.org/
[lcov]: http://ltp.sourceforge.net/coverage/lcov.php
[Teamcenter]: https://www.plm.automation.siemens.com/en/products/teamcenter/
[Openshift]: https://www.openshift.com/

The repository contains the ```Dockerfile``` to extend the official Jenkins docker image adding:

* [cmake]
* [cmocka]
* [lcov]
* [Teamcenter] ```include```  and ```lib``` directories for compilation

in order to prepare Jenkins to compile cross-platform [Teamcenter] server side
code and unit testing code.

However the creation of the image will be triggered running the script, due to some
additional validations that the script does.

## Image creation

### Requirements

* ```unzip``` command accessible from command line
* ```docker``` command accessible from command line
* [Teamcenter] zip official installation package (2nd of the two available). It can be downloaded from the official Siemens Teamcenter software repository.

### Creating the image

The project provides two different ```Dockerfile``` to create the image:

* ```Dockerfile.ubuntu``` based on the latest version of the official Jenkins image, running in Ubuntu.
* ```Dockerfile.4openshift``` based on the official Jenkins image developed for [Openshift], to be deployed in a [Openshift] cloud.

To create the image using any of the two Dockerfiles:

1. clone the project to a directory of your choice
1. make the script executable

        $> chmod 755 script.sh

1. Check that the commands ```unzip``` and ```docker``` are available.
1. Download the [Teamcenter] zip official installation package (2 of 2).
1. Run the script

        $> ./script.sh <Dockerfile> <Path to Teamcenter zip installation package>

The script will unzip some packages from [Teamcenter] zip installation package
and trigger the build of the Docker image. The image is tagged as ```jenkins4tc:latest```. Afterwards a container can be created from the image:

        $> docker run -d --name jenkins4tc -p 8080:8080 -p 50000:50000 jenkins4tc:latest

### Configuration

#### Exposed ports

The image is exposing the ports ```8080``` and ```50000```, as they are already exposed in the images taken as reference.

#### Environment variables

Within the container it has been set the variable ```TC_ROOT```, pointing to the
directory where the [Teamcenter] libraries and include files have been placed.
Any build script that would be triggered by Jenkins can make use of this variable
to find the include files and libraries.
