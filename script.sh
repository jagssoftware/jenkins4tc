#!/bin/bash

#
# (c) 2018 - Jose A. Garcia Sanchez
#

if [ ! -x "$(which unzip)" ]; then
    echo "<unzip> command is required (and must be accessible from command line. Exiting."
    exit -1
fi

if [ ! -x "$(which docker)" ]; then
    echo "<docker> command is required (and must be accessible from command line. Exiting."
    exit -1
fi

if [ $# -lt 2 ]; then
    echo "Specify the Dockerfile and Teamcenter zip official installation package as argument:"
    echo "$0 <Dockerfile> <Teamcenter package>.zip"
    exit -1
fi

if [ ! -f $1 ]; then
    echo "Dockerfile [$1] not found. Please specify the Dockerfile to be used."
    exit -1
fi

if [ ! -f $2 ]; then
    echo "Package [$2] not found. Please specify the package (2 of 2) of Teamcenter installation"
    exit -1
fi

DOCKERFILE=$1
TC_PACKAGE=$2
TC_ROOT=tc_root

echo "Package [$TC_PACKAGE] has been found. Unzipping..."
echo "... include.zip"
unzip -q "$TC_PACKAGE" tc/include.zip
echo "... include_cpp.zip"
unzip -q "$TC_PACKAGE" tc/include_cpp.zip
echo "... lib.zip"
unzip -q "$TC_PACKAGE" tc/lib.zip

echo "Building docker image..."
docker build -t jenkins4tc:latest --force-rm --rm -f $DOCKERFILE .

echo "Removing useless files..."
rm -rf tc

echo "---- [END] ----"
